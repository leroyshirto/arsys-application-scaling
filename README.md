Arsys Application Scaler
====================

This is a simple CPU/RAM scaler for the Arsys/1&1 Cloud Server platform. It uses configured thresholds and limits to automatically increase CPU and RAM settings, to dynamically deal with increases in system load.


Installation
------------------
AAS requires Python 2.7, and the following libraries:

  * [netifaces] 0.10.4
  * [psutil] 3.0.1
  * [requests] 2.5.3

The best way to install these is using [pip]. [virtualenv] is also recommended.

Once Python and pip are installed, clone this repository:

``git clone https://bitbucket.org/leroyshirto/arsys-application-scaling.git``

On linux, the python development packages will be required:

``apt-get install python-dev``

For Ubuntu and:

``yum install python-devel``

For Centos 7 

You should then be able to install the required dependencies using pip:

``pip install -r requirements.txt``


Configuration
------------------
There is an example configuration file, config.cfg, which contains the following options:

  * cpu_upper_threshold - This is the CPU percentage at which scaling will occur
  * cpu_increment - The number of CPUs to increase by each time scaling occurrs
  * cpu_limit - The manimum number of CPUs to scale to. At this point, no additional CPUs will be added
  * mem_upper_threshold = This is the RAM usage percentage at which scaling will occur
  * ram_increment = The number of GBs or RAM to increase by each time scaling occurrs. This is specified in 0.5 increments
  * ram_limit = The manimum amount of RAM in GB to scale to. At this point, no additional RAM will be added

There also need to be an addional config file with your api key. This needs to be called api.cfg and contain the following contant:

```
[api]
key = your_api_key
```

Usage
------------------
The scaler can be run using:
``python program.py``


Notes
------------------
This system relies on hot add capability in guest OS. The currently supported OSs are as follows (with caveats):

Windows Server 2012: works without issue

CentOS 7:CPU hot add works without issue, RAM does not automatically online.  The following [KB article][ubuntuhotcpu] provides additional info

Ubuntu 14.04: CPU requires addition configuration to bring online. The following [KB article][linuxhotram] provides additional info. Ubuntu also has the same RAM issue as CentOS 7

[psutil]: https://github.com/giampaolo/psutil/
[netifaces]: https://pypi.python.org/pypi/netifaces/
[requests]: http://docs.python-requests.org/
[pip]: https://pip.pypa.io/
[virtualenv]: https://virtualenv.pypa.io/
[ubuntuhotcpu]: http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1015501
[linuxhotram]: http://kb.vmware.com/selfservice/microsites/search.do?language=en_US&cmd=displayKC&externalId=1012764