import psutil, time, os, argparse, ConfigParser

from aas.system_info import SystemPerformance, SystemInformation
from aas.api import CloudApiClient

username = password = platform = None
parser = argparse.ArgumentParser(description='Arsys Auto-Scaler')
parser.add_argument('--apikey')
args = parser.parse_args()

config = ConfigParser.ConfigParser()
config.read('config.cfg')
config.read('api.cfg')




def cls():
    os.system(['clear','cls'][os.name == 'nt'])

spinner = ['|', '/', '-', '\\']
spinnerPos = 0
def get_spinner():
	global spinnerPos
	spinnerPos = (spinnerPos + 1) % len(spinner)
	return spinner[spinnerPos]

if config.has_option('api', 'key'):
	api_key = config.get('api', 'key')
# api_key =  args.apikey
# server_id = args.server_id
api = CloudApiClient(api_key)
perf = SystemPerformance()
info = SystemInformation()	

interface_ips = info.interface_ips()
interface_ips.append('82.223.27.137')


for ip in interface_ips:
	server_id = api.get_server_id(server_ip=ip)
	if server_id:
		break

if not server_id:
	raise Exception("Current server is not in your cloud panel")
print "ServerID" + server_id

cpu_upper_threshold = float(config.get('scaling', 'cpu_upper_threshold'))
cpu_increment = float(config.get('scaling', 'cpu_increment'))
cpu_limit = float(config.get('scaling', 'cpu_limit'))
mem_upper_threshold = float(config.get('scaling', 'mem_upper_threshold'))
ram_increment = float(config.get('scaling', 'ram_increment'))
ram_limit = float(config.get('scaling', 'ram_limit'))

def raise_ram_spec():
	system_ram = info.mem_amount()
	api_ram = float(api.get_server_spec(server_id)['ram'])
	target_ram = system_ram + ram_increment

	if target_ram > ram_limit:
		print "Hit RAM Limit"
		return

	#Wait for any pending ram changes to apply
	while system_ram != api_ram:
		time.sleep(0.5)
		system_ram = info.mem_amount()
		api_ram = float(api.get_server_spec(server_id)['ram'])
	print "Setting RAM spec to %s" % (target_ram)
	print api.set_server_spec(server_id, ram=target_ram)

	while target_ram != system_ram:
		print "waiting for target:%s to equal system:%s" % (target_ram, system_ram) 
		time.sleep(0.5)
		system_ram = info.mem_amount()
	time.sleep(10)

def raise_cpu_spec():
	system_cpu = info.cpu_count()
	api_cpu = int(api.get_server_spec(server_id)['vcore'])
	target_cpu = system_cpu + cpu_increment
	if target_cpu > cpu_limit:
		print "Hit CPU Limit"
		return

	#Wait for any pending ram changes to apply
	while system_cpu != api_cpu:
		time.sleep(0.5)
		system_cpu = info.cpu_count(server_id)
		api_cpu = int(api.get_server_spec()['vcore'])
	

	print "Setting CPU spec to %s" % (target_cpu)
	print api.set_server_spec(server_id, cpu_count=target_cpu)

	while target_cpu != system_cpu:
		cls()
		print "waiting for target:%s to equal system:%s %s" % (target_cpu, system_cpu, get_spinner()) 
		time.sleep(0.5)	
		system_cpu = info.cpu_count()
	time.sleep(10)


while(True):
	cpu_percent = perf.cpu_percent()
	cls()
	print "CPU Percent: %-2.2f" % cpu_percent
	if(cpu_percent > cpu_upper_threshold):
		print "CPU Over Threshold, Getting More Spec"
		raise_cpu_spec()	

	mem_percent = perf.mem_percent()
	print "MEM Percent: " + str(mem_percent)
	if(mem_percent > mem_upper_threshold):
		print "RAM Over Threshold, Getting More Spec"
		raise_ram_spec()
	print get_spinner()
	time.sleep(0.5)