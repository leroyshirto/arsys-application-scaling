import requests, json
import pdb
api_url = 'https://cloudpanel-api.1and1.com/v1'

class CloudApiClient:

	def __init__(self, api_token):
		self.api_token = api_token
		self.headers = {
			'content-type' : 'application/json',
			'X-TOKEN' : api_token
		}

	def get_server_spec(self, server_id):
		url = api_url + '/servers/' + server_id + '/hardware'
		r = requests.get(url, headers=self.headers)
		return r.json()

	def get_server_status(self, server_id):
		url = api_url + '/servers/' + server_id + '/status'
		r = requests.get(url, headers=self.headers)
		return r.json()

	def get_server(self, server_id):
		url = api_url + '/servers/' + server_id
		r = requests.get(url, headers=self.headers)
		return r.json()

	def get_server_id(self, server_cp_id = None, server_ip=None):
		url = api_url + '/servers'
		r = requests.get(url, headers=self.headers)
		server_list = r.json()
		for server in server_list:
			if server_ip:
				for ip in server['ips']:
					if ip['ip'] == server_ip:
						return server['id']

			if server_cp_id:
				r = requests.get(url + '/' + server['id'], headers=self.headers)
				server_json = r.json()
				if server_json['cloudpanel_id'] == server_cp_id:
					return server_json['id']

	def set_server_spec(self,server_id, cpu_count = None, ram = None):
		url = api_url + '/servers/' + server_id + '/hardware'
		payload = {}
		if cpu_count:
			payload['vcore'] = int(cpu_count)
			payload['cores_per_processor'] = 1
		if ram:
			payload['ram'] = float(ram)
		print payload
		r = requests.put(url, data=json.dumps(payload), headers=self.headers)
		return r.json()

	def power_off_server(self, server_id):
		return self.process_server_action(server_id, 'POWER_OFF')

	def power_on_server(self, server_id):
		return self.process_server_action(server_id, 'POWER_ON')

	def reboot_server(self, server_id):
		return self.process_server_action(server_id, 'REBOOT')

	def process_server_action(self, server_id, action):
		url = api_url + '/servers/' + server_id + '/status/action'
		payload = {
		  "action": action,
		  "method": "SOFTWARE"
		}
		r = requests.put(url, data=json.dumps(payload), headers=self.headers)
		return r.json()



