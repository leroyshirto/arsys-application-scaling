import psutil, math

from netifaces import interfaces, ifaddresses, AF_INET

class SystemPerformance:
	cpu_averaged_value = 0.0
	def cpu_percent(self):
		cpu_current_value = psutil.cpu_percent()
		self.cpu_averaged_value = (self.cpu_averaged_value + cpu_current_value) / 2
		return self.cpu_averaged_value


	def mem_percent(self):
		return psutil.virtual_memory().percent



class SystemInformation:

	def cpu_count(self):
		psutil.cpu_count.cache_clear()
		return psutil.cpu_count()

	def mem_amount(self):
		return  float(0.5 * math.ceil(2.0 * float(psutil.virtual_memory().total) / (1 << (3)*10)))

	def interface_ips(self):
		ips = []
		for ifaceName in interfaces():
			if AF_INET in ifaddresses(ifaceName):
				address = ifaddresses(ifaceName)[AF_INET][0]['addr']
				if address != '' and address != '127.0.0.1' and (address not in ips):
					ips.append(address)
		return ips